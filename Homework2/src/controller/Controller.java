package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import models.Factory;
import view.GUI;

public class Controller {
	public static GUI g;
	
	public Controller(GUI g) {
		this.g = g;
		this.g.addRunListener(new Run());
		this.g.addClearListener(new Clear());
	}
	
	public class Run implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int nrOfQueues=Integer.parseInt(g.getTextField().getText());
			int nrOfClients=Integer.parseInt(g.getTextField_1().getText());
			int arrMin=Integer.parseInt(g.getTextField_2().getText());
			int arrMax=Integer.parseInt(g.getTextField_3().getText());
			int servMin=Integer.parseInt(g.getTextField_4().getText());
			int servMax=Integer.parseInt(g.getTextField_5().getText());
			int simulation=Integer.parseInt(g.getTextField_6().getText());
			Factory f = new Factory(nrOfQueues, nrOfClients, arrMin, arrMax, servMin, servMax, simulation);
			Thread th = new Thread(f);
			th.start();
		}
		
	}
	
	public class Clear implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			g.getTextField().setText("");
			g.getTextField_1().setText("");
			g.getTextField_2().setText("");
			g.getTextField_3().setText("");
			g.getTextField_4().setText("");
			g.getTextField_5().setText("");
			g.getTextField_6().setText("");
			g.getTextArea().setText("");
			g.getTextArea_1().setText("");
		}
		
	}
}
