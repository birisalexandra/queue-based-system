package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JScrollBar;
import javax.swing.ScrollPaneConstants;

public class GUI extends JFrame {

	private JPanel contentPane =  new JPanel();
	private JTextField textField = new JTextField();
	private JTextField textField_1 = new JTextField();
	private JTextField textField_2 = new JTextField();
	private JTextField textField_3 = new JTextField();
	private JTextField textField_4 = new JTextField();
	private JTextField textField_5 = new JTextField();
	private JTextField textField_6 = new JTextField();
	private JTextArea textArea_1 = new JTextArea();
	private JButton btnRun = new JButton("Run");
	private JButton btnClear = new JButton("Clear");
	private final JScrollPane scrollPane = new JScrollPane();
	private final JTextArea textArea = new JTextArea();
	
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 571, 390);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNumberOfQueues = new JLabel("Number of queues");
		lblNumberOfQueues.setBounds(10, 11, 140, 14);
		contentPane.add(lblNumberOfQueues);
		
		textField.setBounds(136, 8, 37, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNumberOfClients = new JLabel("Number of clients");
		lblNumberOfClients.setBounds(10, 39, 125, 14);
		contentPane.add(lblNumberOfClients);
		
		textField_1.setBounds(136, 36, 37, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblArrivalTime = new JLabel("Arrival Time");
		lblArrivalTime.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblArrivalTime.setBounds(61, 64, 89, 17);
		contentPane.add(lblArrivalTime);
		
		JLabel lblMin = new JLabel("Min");
		lblMin.setBounds(21, 92, 49, 14);
		contentPane.add(lblMin);
		
		JLabel lblMax = new JLabel("Max");
		lblMax.setBounds(104, 92, 56, 14);
		contentPane.add(lblMax);
		
		textField_2.setBounds(46, 89, 37, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3.setBounds(136, 89, 37, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblServicetime = new JLabel("ServiceTime");
		lblServicetime.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblServicetime.setBounds(61, 120, 83, 17);
		contentPane.add(lblServicetime);
		
		JLabel lblMin_1 = new JLabel("Min");
		lblMin_1.setBounds(21, 148, 49, 14);
		contentPane.add(lblMin_1);
		
		textField_4.setBounds(46, 145, 37, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblMax_1 = new JLabel("Max");
		lblMax_1.setBounds(104, 148, 56, 14);
		contentPane.add(lblMax_1);
		
		textField_5.setBounds(136, 145, 37, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblSimulationTime = new JLabel("Simulation Time");
		lblSimulationTime.setBounds(10, 182, 140, 28);
		contentPane.add(lblSimulationTime);
		
		textField_6.setBounds(136, 186, 37, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		btnRun.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnRun.setBounds(61, 235, 89, 23);
		contentPane.add(btnRun);
		
		btnClear.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnClear.setBounds(61, 269, 89, 23);
		contentPane.add(btnClear);
		
		textArea_1.setBounds(212, 217, 334, 117);
		contentPane.add(textArea_1);
		scrollPane.setBounds(214, 11, 332, 197);
		
		contentPane.add(scrollPane);
		
		scrollPane.setViewportView(textArea);
	}

	//adds action listener for each button
	public void addRunListener(ActionListener a) {
		btnRun.addActionListener(a);
	}
	
	public void addClearListener(ActionListener a) {
		btnClear.addActionListener(a);
	}
	
	public void setTextArea(String s) {
		this.textArea.append(s);
	}
	
	public void setTextArea_1(String s) {
		this.textArea_1.append(s);
	}

	public JTextField getTextField() {
		return textField;
	}

	public JTextField getTextField_1() {
		return textField_1;
	}

	public JTextField getTextField_2() {
		return textField_2;
	}

	public JTextField getTextField_3() {
		return textField_3;
	}

	public JTextField getTextField_4() {
		return textField_4;
	}

	public JTextField getTextField_5() {
		return textField_5;
	}

	public JTextField getTextField_6() {
		return textField_6;
	}
	
	public JTextArea getTextArea() {
		return textArea;
	}
	
	public JTextArea getTextArea_1() {
		return textArea_1;
	}
}
