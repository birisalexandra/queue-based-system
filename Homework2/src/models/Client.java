package models;

public class Client {
	private int id;
	private int arrivalTime;
	private int serviceTime;
	
	public Client(int id, int at, int st) {
		this.id = id;
		this.arrivalTime = at;
		this.serviceTime = st;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public synchronized int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public synchronized int getServiceTime() throws InterruptedException {
		return serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}
}
