package models;

import java.util.ArrayList;
import java.util.Random;

import controller.Controller;

public class Factory implements Runnable {
	private ArrayList<Client> clients;
	private ArrayList<Queue> queues;
	private ArrayList<Thread> threads;
	private int nrOfQueues;
	private int nrOfClients;
	private int arrMin, arrMax;
	private int servMin, servMax;
	static int currentTime = 0;
	private int simulationTime; //when the thread stops
	
	public Factory(int nrQ, int nrC, int arrMin, int arrMax, int servMin, int servMax, int simulation) {
		this.clients = new ArrayList<Client>();
		this.queues = new ArrayList<Queue>();
		this.threads = new ArrayList<Thread>();
		this.nrOfQueues = nrQ;
		this.nrOfClients = nrC;
		this.arrMin = arrMin;
		this.arrMax = arrMax;
		this.servMin = servMin;
		this.servMax = servMax;
		this.simulationTime = simulation;
		
		for (int i = 0; i < nrOfQueues; i++) {
			Queue c = new Queue(i); //we generate a new queue
			queues.add(c); //we add the queue created to the list of queues
			Thread th = new Thread(c); //we generate a thread for the queue
			threads.add(th); //we add the thread to the list of threads
			threads.get(i).start(); //we start the thread
		}
	}
	
	public synchronized int shortestQueue() { //we find the queue with the minimum number of clients
		int id = 0;
		int min = queues.get(0).size(); //min takes the value of the first queue
		for (int i = 1; i < nrOfQueues; i++) //we compare the first value with the rest
			if (queues.get(i).size() < min) {
				min = queues.get(i).size();
				id = i;
			}
		return id;
	}
	
	public synchronized void addClients(int nrOfClients) {//we generate the clients using random numbers
		Random random = new Random();
		int arrTime = 0;
		int servTime = 0;
		for (int i = 0; i < nrOfClients; i++) {
			arrTime = arrTime + random.nextInt((arrMax - arrMin) + 1) + arrMin;
			servTime = random.nextInt((servMax - servMin) + 1) + servMin;
			Client c = new Client(i + 1, arrTime, servTime);
			clients.add(c);
		}
	}
	
	public synchronized void emptyTime(int[] data) { //at each step we calculate which queues are empty
		for (Queue q: queues) 
			if (q.size() == 0) 
				data[q.getId()]++;
	}
	
	@Override
	public void run() { 
		try {
			boolean print = true; //to control the text to be printed just once
			int avgServiceTime = 0, avgWaitingTime = 0;
			int maxi = 0, t = 0;
			int[] data = new int[nrOfQueues]; 
			addClients(nrOfClients); //we generate the clients
			while (true) {
				if (!clients.isEmpty()) {
					if (currentTime == clients.get(0).getArrivalTime()) { 
						int index = shortestQueue();
						queues.get(index).addClient(clients.get(0)); //we add the client to the shortest queue
						avgServiceTime += clients.get(0).getServiceTime();
						avgWaitingTime += queues.get(index).getWaitingTime();
						for (Queue q: queues) { //part used to determine the peak hour
							int sum = 0;
							sum += q.size();
							if (sum > maxi) { //we save the maximum number of clients that are in the shop and the time t 
								maxi = sum;
								t = Factory.currentTime;
							}
						}
						try {
							Controller.g.setTextArea("Client:" + clients.get(0).getId() + " Casa:" + queues.get(index).getId() + " arrived at:" + clients.get(0).getArrivalTime() + " with service time:" + clients.get(0).getServiceTime() + "\n");
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						clients.remove(0);
					}
					emptyTime(data);
					currentTime++;
					if (currentTime == simulationTime) { //error when the simulation time is too small and there are still clients on queues
						for (Thread th: threads) //we stop the threads when the simulation time ends
							th.interrupt();
						if (!clients.isEmpty())
							Controller.g.setTextArea_1("Simulation time too small!\n");
					}
				}
				else {
					if(print == true) { 
						Controller.g.setTextArea_1("Average Service Time: " + avgServiceTime/nrOfClients + "\n");
						Controller.g.setTextArea_1("Average Waiting Time: " + avgWaitingTime/nrOfClients + "\n");
						Controller.g.setTextArea_1("Peak hour time: " + t + " with " + maxi + " clients\n");
						for(int i = 0; i < nrOfQueues; i++) {
							Controller.g.setTextArea_1("Queue number: " + i + " empty for: "+ data[i] + "\n");
						}
						print = false;
					}
					for(Thread th: threads) //we stop the threads when there are no clients anymore
						th.interrupt();
				}
				Thread.sleep(500);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
