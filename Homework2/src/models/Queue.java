package models;

import java.util.ArrayList;

import controller.Controller;


public class Queue implements Runnable{
	private ArrayList<Client> clients; //each queue has a list of clients that need to be served
	private int id;
	
	public Queue(int id) {
		this.clients = new ArrayList<Client>();
		this.id = id;
	}
	
	public ArrayList<Client> getClienti() {
		return clients;
	}

	public int getId() {
		return id;
	}
	
	public synchronized void addClient(Client c) throws InterruptedException {
		clients.add(c);
		notifyAll(); //wakes up all the threads that called wait() on the same object
	}
	
	public synchronized void removeClient() throws InterruptedException { //we use the InterruptedException to be able to perform the wait operation
		while(clients.size() == 0) {
			wait(); //tells the calling thread to give up the monitor and go to sleep until some other thread enters the same monitor
		}
		clients.remove(0);
		notifyAll();
	}
	
	public synchronized int size() { //method needed to see which queue has fewer people
		return clients.size();
	}
	
	public synchronized int getWaitingTime() throws InterruptedException { //we compute for each queue the time clients need to wait to leave the shop
		int waitingTime = 0;
		for(Client c: clients)
			waitingTime += c.getArrivalTime() + c.getServiceTime() - Factory.currentTime; 
		return waitingTime;
	}
	
	@Override
	public void run() {
		try{
			while(true) {
				if(!clients.isEmpty()) {
					if(Factory.currentTime == clients.get(0).getServiceTime() + clients.get(0).getArrivalTime()) { //we remove each client after the service time is over
						Controller.g.setTextArea("Client:" + clients.get(0).getId() + " has left\n");
						removeClient();
					}
				}
				else 
					Thread.sleep(500);
			}
		}
		catch(InterruptedException e) {
			e.printStackTrace();
		}
	}
}